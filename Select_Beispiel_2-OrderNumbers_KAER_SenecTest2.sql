--select DBMS_XMLGEN.GETXML('select ''Markus'' "Name" from dual') "DBMS_XMLGEN.GETXML" from dual;
--select DBMS_XMLGEN.GETXMLTYPE('select ''Markus'' "Name" from dual') "DBMS_XMLGEN.GETXMLTYPE" from dual;
--> https://docs.oracle.com/database/121/ADXDB/xdb03usg.htm#ADXDB4163
--> https://docs.oracle.com/database/121/ADXDB/xdb13gen.htm#ADXDB4994
select '<?xml version="1.0" encoding="UTF-8"?>' ||
    XMLElement(
        "KiSoft_KOMMBESTR"
        ,XMLElement(
            "ApplicationArea"
            ,XMLForest(to_char(sysdate, 'YYYY-MM-DD HH24:MI:SS') "CreationDateTime")
            ,XMLForest(SYS_GUID() "MessageID")
            ,XMLForest('SS_KOMMBESTR' "MessageType")
            ,XMLForest('KiSoft' "Sender")
            ,XMLForest('KAE' "Receiver")
            ,XMLForest('SK' "WMSlocation")
            ,XMLForest('SENEC2' "WMSbranch")
            ,XMLForest('KAE' "WMScompanyID")
        )
        ,XMLElement(
            "DataArea"
            ,(select
                XMLAgg(
                    XMLElement(
                        "Order"
                        ,XMLElement(
                            "SS_KOMMBESTR"
                            ,XMLForest(ss_kommbestr.mandant "h_mandant")
                            ,XMLForest(ss_kommbestr.bestellnr "h_bestellnr")
                            ,XMLForest(ss_kommbestr.bestelltyp "h_bestelltyp")
                            ,XMLForest(ss_kommbestr.kundennr "h_kundennr")
                            ,XMLForest(ss_kommbestr.klient "h_klient")
                            ,XMLForest(ss_kommbestr.name "h_name")
                            ,XMLForest(null "h_kuerzel")
                            ,XMLForest(ss_kommbestr.plz "h_plz")
                            ,XMLForest(ss_kommbestr.ort "h_ort")
                            ,XMLForest(ss_kommbestr.strasse "h_strasse")
                            ,XMLForest(ss_kommbestr.land "h_land")
                            ,XMLForest(ss_kommbestr.spediteur "h_spediteur")
                            ,XMLForest(null "h_spediteur2")
                            ,XMLForest(ss_kommbestr.verladedatum "h_verladedatum")
                            ,XMLForest(ss_kommbestr.tournr "h_tournr")
                            ,XMLForest(null "h_auslieferer")
                            ,XMLForest(ss_kommbestr.posanz "h_posanz")
                            ,XMLForest(ss_kommbestr.info "h_info")
                            ,XMLForest(ss_kommbestr.status "h_status")
                            ,XMLForest(null "h_fehlercode")
                            ,XMLForest(null "h_wator")
                            ,XMLForest(ss_kommbestr.bereitzeit "h_bereitzeit")
                            ,XMLForest(ss_kommbestr.startzeit "h_startzeit")
                            ,XMLForest(ss_kommbestr.endezeit "h_endzeit")
                            ,XMLForest(ss_kommbestr.pause "h_pause")
                            ,XMLForest(null "h_lagerkosten")
                            ,XMLForest(null "h_lieferseq")
                            ,XMLForest(ss_kommbestr.lieferseqstatus "h_lieferseqstatus")
                            ,XMLForest(ss_kommbestr.auftraggeber "h_auftraggeber")
                            ,XMLForest(ss_kommbestr.planzeit "h_planzeit")
                            ,XMLForest(ss_kommbestr.timestamp "h_timestamp")
                            ,XMLForest(null "h_batchseq")
                            ,XMLForest(null "h_errorcode")
                            ,XMLForest(null "h_errinfo")
                            ,XMLForest(ss_kommbestr.name2 "h_name2") --> Element nicht in XSD-Schema vorhanden!
                            ,XMLForest(ss_kommbestr.name3 "h_name3")--> Element nicht in XSD-Schema vorhanden!
                            ,XMLForest(ss_kommbestr.namezusatz "h_namezusatz")--> Element nicht in XSD-Schema vorhanden!
                            ,(select
                                XMLAgg(
                                    XMLElement(
                                        "SS_IFADDON"
                                        ,XMLForest('HEADTXT' "h_aaa")
                                        ,XMLForest(ss_kommbestr.mandant "h_mandant")
                                        ,XMLForest(null "h_refdata_type")
                                        ,XMLForest(ss_kommbestr.bestellnr "h_refdata_key1")
                                        ,XMLForest(ss_kommbestr.klient "h_refdata_key2")
                                        ,XMLForest(null "h_refdata_key3")
                                        ,XMLForest(null "h_refdata_key4")
                                        ,XMLForest(null "h_refdata_key5")
                                        ,XMLForest(null "h_lineno")
                                        ,XMLForest(null "h_info")
                                        ,XMLForest(null "h_confirm_info")
                                        ,XMLForest(null "h_allow_feedback")
                                        ,XMLForest(null "h_status")
                                        ,XMLForest(null "h_timestamp")
                                        ,XMLForest(null "h_batchseq")
                                        ,XMLForest(null "h_errorcode")
                                        ,XMLForest(null "h_errinfo")
                                    )
                                )
                                from ( --> SS_KOMMBESTR_INFO_KAE
                                    select *
                                    from dual
                                )
                            )
                        )
                        ,(select
                            XMLAgg(
                                XMLElement(
                                    "SS_KOMMBESTPR"
                                    ,XMLForest(ss_kommbestpr.mandant "i_mandant")
                                    ,XMLForest(ss_kommbestpr.bestellnr "i_bestellnr")
                                    ,XMLForest(ss_kommbestpr.posnr "i_posnr")
                                    ,XMLForest(null "i_bestelltyp")
                                    ,XMLForest(null "i_referenznr")
                                    ,XMLForest(null "i_lfdne")
                                    ,XMLForest(null "i_kundennr")
                                    ,XMLForest(null "i_partnerklient")
                                    ,XMLForest(null "i_persnr")
                                    ,XMLForest(null "i_ersatzartikel")
                                    ,XMLForest(null "i_urposnr")
                                    ,XMLForest(null "i_lonr")
                                    ,XMLForest(null "i_sammellonr")
                                    ,XMLForest(null "i_liefernr")
                                    ,XMLForest(null "i_lsposnr")
                                    ,XMLForest(null "i_verladenr")
                                    ,XMLForest(null "i_lieferseq")
                                    ,XMLForest(null "i_artnr")
                                    ,XMLForest(null "i_artklient")
                                    ,XMLForest(null "i_var")
                                    ,XMLForest(null "i_bestart")
                                    ,XMLForest(null "i_artikeltyp")
                                    ,XMLForest(null "i_be1he")
                                    ,XMLForest(null "i_hebez")
                                    ,XMLForest(null "i_barcode")
                                    ,XMLForest(null "i_be1bez")
                                    ,XMLForest(null "i_bestellmenge1")
                                    ,XMLForest(null "i_istmenge")
                                    ,XMLForest(null "i_storno1")
                                    ,XMLForest(null "i_be2he")
                                    ,XMLForest(null "i_istmenge2")
                                    ,XMLForest(null "i_storno2")
                                    ,XMLForest(null "i_bruttogew")
                                    ,XMLForest(null "i_fehltyp")
                                    ,XMLForest(null "i_istmhd")
                                    ,XMLForest(null "i_seriennr")
                                    ,XMLForest(null "i_charge")
                                    ,XMLForest(null "i_info")
                                    ,XMLForest(null "i_wator")
                                    ,XMLForest(null "i_buchtyp")
                                    ,XMLForest(null "i_rueckmeldtyp")
                                    ,XMLForest(null "i_lagerkosten")
                                    ,XMLForest(null "i_leergut")
                                    ,XMLForest(null "i_erfasspersnr")
                                    ,XMLForest(null "i_status")
                                    ,XMLForest(null "i_timestamp")
                                    ,XMLForest(null "i_batchseq")
                                    ,XMLForest(null "i_errcode")
                                    ,XMLForest(null "i_errinfo")
                                    ,XMLForest(null "i_preis1")
                                    ,XMLForest(null "i_preis2")
                                    ,XMLForest(null "i_barcode1")
                                    ,XMLForest(null "i_barcode2")
                                    ,XMLForest(null "i_barcode3")
                                    ,XMLForest(null "i_barcode4")
                                    ,XMLForest(null "i_barcode5")
                                    ,XMLForest(null "i_barcode6")
                                    ,XMLForest(null "i_barcode7")
                                    ,XMLForest(null "i_barcode8")
                                    ,XMLForest(null "i_barcode9")
                                    ,XMLForest(null "i_barcode10")
                                    ,(select
                                        XMLAgg(
                                            XMLElement(
                                                "SS_IFADDON"
                                                ,XMLForest('POSTXT' "h_aaa")
                                                ,XMLForest(ss_kommbestpr.mandant "h_mandant")
                                                ,XMLForest(null "h_refdata_type")
                                                ,XMLForest(ss_kommbestpr.bestellnr "h_refdata_key1")
                                                ,XMLForest(ss_kommbestpr.partnerklient "h_refdata_key2")
                                                ,XMLForest(null "h_refdata_key3")
                                                ,XMLForest(ss_kommbestpr.posnr "h_refdata_key4")
                                                ,XMLForest(null "h_refdata_key5")
                                                ,XMLForest(null "h_lineno")
                                                ,XMLForest(null "h_info")
                                                ,XMLForest(null "h_confirm_info")
                                                ,XMLForest(null "h_allow_feedback")
                                                ,XMLForest(null "h_status")
                                                ,XMLForest(null "h_timestamp")
                                                ,XMLForest(null "h_batchseq")
                                                ,XMLForest(null "h_errorcode")
                                                ,XMLForest(null "h_errinfo")
                                            )
                                        )
                                        from ( --> SS_KOMMBESTPR_INFO_KAE
                                            select * from dual union all
                                            select * from dual
                                        )
                                    )
                                )
                            )
                            from /*( --> SS_KOMMBESTPR_KAE
                                select 'D' mandant, '0975217708' bestellnr, 'KAE' partnerklient, 10 posnr, 'PCS' artikeltyp, null dummy from dual union all
                                select 'D' mandant, '0975217708' bestellnr, 'KAE' partnerklient, 20 posnr, 'PCS' artikeltyp, null dummy from dual
                            )*/ ss_kommbestpr
                            where ss_kommbestpr.bestellnr = ss_kommbestr.bestellnr
                                and ss_kommbestpr.partnerklient = ss_kommbestr.klient
                                and ss_kommbestpr.artikeltyp = 'PCS'
                        )
                    )
                )
                from /* ( --> SS_KOMMBESTR_KAE
                    select 'D' mandant, '0975207596' bestellnr, 'NORMAL' bestelltyp, null kundennr, 'KAE' klient, null name, null plz, null ort, null strasse, null land, null spediteur, null verladedatum, null tournr, null posanz, null info, 'NEW' status, null bereitzeit, null startzeit, null endzeit, null pause, null lieferseqstatus ,null auftraggeber, null planzeit, null timestamp, null name2, null name3, null namezusatz from dual union all
                    select 'D' mandant, '0975216410' bestellnr, 'NORMAL' bestelltyp, null kundennr, 'KAE' klient, null name, null plz, null ort, null strasse, null land, null spediteur, null verladedatum, null tournr, null posanz, null info, 'NEW' status, null bereitzeit, null startzeit, null endzeit, null pause, null lieferseqstatus ,null auftraggeber, null planzeit, null timestamp, null name2, null name3, null namezusatz from dual union all
                    select 'D' mandant, '0975216413' bestellnr, 'NORMAL' bestelltyp, null kundennr, 'KAE' klient, null name, null plz, null ort, null strasse, null land, null spediteur, null verladedatum, null tournr, null posanz, null info, 'NEW' status, null bereitzeit, null startzeit, null endzeit, null pause, null lieferseqstatus ,null auftraggeber, null planzeit, null timestamp, null name2, null name3, null namezusatz from dual union all
                    select 'D' mandant, '0975216856' bestellnr, 'NORMAL' bestelltyp, null kundennr, 'KAE' klient, null name, null plz, null ort, null strasse, null land, null spediteur, null verladedatum, null tournr, null posanz, null info, 'NEW' status, null bereitzeit, null startzeit, null endzeit, null pause, null lieferseqstatus ,null auftraggeber, null planzeit, null timestamp, null name2, null name3, null namezusatz from dual union all
                    select 'D' mandant, '0975217708' bestellnr, 'NORMAL' bestelltyp, null kundennr, 'KAE' klient, null name, null plz, null ort, null strasse, null land, null spediteur, null verladedatum, null tournr, null posanz, null info, 'NEW' status, null bereitzeit, null startzeit, null endzeit, null pause, null lieferseqstatus ,null auftraggeber, null planzeit, null timestamp, null name2, null name3, null namezusatz from dual union all
                    select 'D' mandant, '0975218143' bestellnr, 'NORMAL' bestelltyp, null kundennr, 'KAE' klient, null name, null plz, null ort, null strasse, null land, null spediteur, null verladedatum, null tournr, null posanz, null info, 'NEW' status, null bereitzeit, null startzeit, null endzeit, null pause, null lieferseqstatus ,null auftraggeber, null planzeit, null timestamp, null name2, null name3, null namezusatz from dual
                ) */ ss_kommbestr
                where 
                    -- ss_kommbestr.status = 'NEW'
                    ss_kommbestr.bestellnr in ('0975070987','0975174955')
                    and ss_kommbestr.klient = 'KAE'
            )
        )
    ).GetClobVal() as XML
from dual;