CREATE OR REPLACE PROCEDURE PRC_SCH_SWORD_KOMMBESTR 
(
  V_BESTELLNR IN VARCHAR2 
, V_KLIENT IN VARCHAR2 
) AS 

  v_Logging   VARCHAR2(2000); --Variable f�rs Logging
  V_BATCHSEQ SS_KOMMBEST.BATCHSEQ%TYPE;
  
BEGIN


BEGIN
  select batchseq
  into V_BATCHSEQ
  from ss_kommbest
  where bestellnr = V_BESTELLNR
    and status = 'DONE'
    and rownum = 1;
    
  EXCEPTION
    WHEN OTHERS THEN
      log_in_db.logerror('ERR',SQLERRM, 'PRC_SCH_SWORD_KOMMBESTR', 'Ermittlung BATCHSEQ: '||v_Logging);
      commit;
      NULL;
      
END;


insert into SWORD_KOMMBESTR
  values (V_BESTELLNR
        , V_KLIENT
        , 'NEW'
        , sqltools.gettimestamp
        , null
        , V_BATCHSEQ
        , F_SCH_SWORD_XML_KOMMBESTR(V_BESTELLNR, V_KLIENT));


 EXCEPTION
    WHEN OTHERS THEN
      log_in_db.logerror('ERR',SQLERRM, 'PRC_SCH_SWORD_KOMMBESTR', 'Insert into SWORD_KOMMBESTR: '||v_Logging);
      commit;
      NULL;        
END PRC_SCH_SWORD_KOMMBESTR;