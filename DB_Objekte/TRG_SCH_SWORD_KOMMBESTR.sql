create or replace TRIGGER TRG_SCH_SWORD_KOMMBESTR 
AFTER UPDATE ON KUBEST 
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW 
WHEN(new.statrueckmeldung = 'FERTIG')
DECLARE
  v_Logging   VARCHAR2(2000); --Variable f�rs Logging
  V_KLIENT    PARTNER.NR%TYPE;
  V_BATCHSEQ  SS_KOMMBEST.BATCHSEQ%TYPE;
  
BEGIN

v_Logging := 'BESTELLNR: '||:new.bestellnr||' KLIENT: '||V_KLIENT;

select klient
into V_KLIENT
from kunde
where kundenid = :new.kundenid;

BEGIN
  select batchseq
  into V_BATCHSEQ
  from ss_kommbest
  where bestellnr = :new.bestellnr
    and status = 'DONE'
    and rownum = 1;
    
EXCEPTION
  WHEN OTHERS THEN
    log_in_db.logerror('ERR',SQLERRM, 'TRG_SCH_SWORD_KOMMBESTR', 'Ermittlung BATCHSEQ: '||v_Logging);
    NULL;
    
END;

  PRC_SCH_SWORD_KOMMBESTR(:new.bestellnr, V_KLIENT);
  
  /*
  insert into SWORD_KOMMBESTR
  values (:new.bestellnr
        , V_KLIENT
        , 'NEW'
        , sqltools.gettimestamp
        , null
        , V_BATCHSEQ
        , F_SCH_SWORD_XML_KOMMBESTR(:new.bestellnr, V_KLIENT));
    */    

EXCEPTION
  WHEN OTHERS THEN
    log_in_db.logerror('ERR',SQLERRM, 'TRG_SCH_SWORD_KOMMBESTR', 'Insert SWORD_KOMMBESTR: '||v_Logging);
    NULL;

END;