CREATE OR REPLACE FUNCTION F_SCH_SWORD_XML_KOMMBESTR 
(
  V_BESTELLNR IN VARCHAR2 
, V_KLIENT IN VARCHAR2 
) RETURN CLOB AS 

  V_XMLOUTPUT CLOB; --output variable
  V_COUNTRY   VARCHAR2(5);
  V_USER      VARCHAR2(20);
  v_Logging   VARCHAR2(2000); --Variable f�rs Logging
  
BEGIN

v_Logging := 'BESTELLNR: '||V_BESTELLNR||' KLIENT: '||V_KLIENT;

select 
      DECODE(V_KLIENT
              --SK KAE
              , 'KAE'           , 'senec2'
      , DECODE(user
                --AT
                , 'STPOELTEN'     , '8003'
                , 'HOERSCHING'    , '8100'
                , 'RIED'          , '8300'
                , 'SALZBURG'      , '8400'
                , 'WERNDORF'      , '8502'
                , 'KLAGENFURT'    , '8600'
                , 'INNSBRUCK'     , '8700'
                , 'KUFSTEIN'      , '8701'
                , 'ROETHIS'       , '8704'
                , 'FREUDENAU'     , '9700'
                , 'WIENALBERN'    , '9705'
                --BG
                , 'SOFIA'         , 'sofia'
                , 'VARNA'         , 'varna'
                --HU
                , 'BUDAPEST_308'  , 'budapest'
                --RO
                , 'CHIAJNA'       , 'chiajna'
                , 'CLUJ'          , 'cluj'
                , 'CUTRICI'       , 'curtici'
                , 'IASI'          , 'iasi'
                , 'TIMISOARA'     , 'timisoara'
                --RS
                , 'BELGRAD'       , 'belgrad'
                --SK
                , 'SENEC'         , 'senec'
                , 'ZILINA_308'    , 'zilina'
              , 'ERR')
            )
into V_USER
from dual;


BEGIN
  select land
  into V_COUNTRY
  from partner
  where partnerid = '-1';

EXCEPTION
  WHEN OTHERS THEN
    log_in_db.logerror('ERR',SQLERRM, 'F_SCH_SWORD_XML_KOMMBESTR', 'Ermittlung V_COUNTRY: '||v_Logging);
    NULL;

END;

SELECT 
    '<?xml version="1.0" encoding="UTF-8"?>' ||
    '<KiSoft_KOMMBESTR>'||
    XMLElement(
            "ApplicationArea"
            ,XMLForest(to_char(sysdate, 'YYYY-MM-DD')||'T'||to_char(sysdate, 'HH24:MI:SS') "CreationDateTime")
            ,XMLForest(SYS_GUID() "MessageID")
            ,XMLForest('SS_KOMMBESTR' "MessageType")
            ,XMLForest('KiSoft' "Sender")
            ,XMLForest(V_KLIENT "Receiver")
            ,XMLForest(V_COUNTRY "WMSlocation")
            ,XMLForest(V_USER "WMSbranch")
            ,XMLForest(V_KLIENT "WMScompanyID")
        ).GetClobVal()
        ||'<DataArea>'
        ||'<Order>'
        ||KOPF.XML_HEAD||POS.XML_POS
        ||'</Order>'
        ||'</DataArea>'
        ||'</KiSoft_KOMMBESTR>'
    INTO V_XMLOUTPUT
FROM
(select MAX(r.bestellnr) as bestellnr
      , MAX(r.klient) as klient
      , XMLAgg(
          XMLElement(
                "SS_KOMMBESTR"
                ,XMLForest(r.mandant "h_mandant")
                ,XMLForest(r.bestellnr "h_bestellnr")
                ,XMLForest(r.bestelltyp "h_bestelltyp")
                ,XMLForest(r.kundennr "h_kundennr")
                ,XMLForest(r.klient "h_klient")
                ,XMLForest(r.name "h_name")
                ,XMLForest(r.kuerzel "h_kuerzel")
                ,XMLForest(r.plz "h_plz")
                ,XMLForest(r.ort "h_ort")
                ,XMLForest(r.strasse "h_strasse")
                ,XMLForest(r.land "h_land")
                ,XMLForest(r.spediteur "h_spediteur")
                ,XMLForest(r.spediteur2 "h_spediteur2")
                ,XMLForest(to_char(r.verladedatum, 'YYYYMMDDHH24MISS') "h_verladedatum")
                ,XMLForest(r.tournr "h_tournr")
                ,XMLForest(r.auslieferer "h_auslieferer")
                ,XMLForest(r.posanz "h_posanz")
                ,XMLForest(r.info "h_info")
                ,XMLForest(r.status "h_status")
                ,XMLForest(r.fehlercode "h_fehlercode")
                ,XMLForest(r.wator "h_wator")
                ,XMLForest(to_char(r.bereitzeit, 'YYYYMMDDHH24MISS') "h_bereitzeit")
                ,XMLForest(to_char(r.startzeit, 'YYYYMMDDHH24MISS') "h_startzeit")
                ,XMLForest(to_char(r.endezeit, 'YYYYMMDDHH24MISS') "h_endzeit")
                ,XMLForest(r.pause "h_pause")
                ,XMLForest(r.lagerkosten "h_lagerkosten")
                ,XMLForest(r.lieferseq "h_lieferseq")
                ,XMLForest(r.lieferseqstatus "h_lieferseqstatus")
                ,XMLForest(r.auftraggeber "h_auftraggeber")
                ,XMLForest(r.planzeit "h_planzeit")
                ,XMLForest(r.timestamp "h_timestamp")
                ,XMLForest(r.batchseq "h_batchseq")
                ,XMLForest(r.errcode "h_errorcode")
                ,XMLForest(r.errinfo "h_errinfo")
                ,XMLForest(r.name2 "h_name2") --> Element nicht in XSD-Schema vorhanden!
                ,XMLForest(r.name3 "h_name3")--> Element nicht in XSD-Schema vorhanden!
                ,XMLForest(r.namezusatz "h_namezusatz")--> Element nicht in XSD-Schema vorhanden!
                ,XMLAGG(
                  XMLElement(
                    "SS_IFADDON"
                    ,XMLForest('HEADTXT' "h_aaa")
                    ,XMLForest(h_info.mandator "h_mandant")
                    ,XMLForest(h_info.refdata_type "h_refdata_type")
                    ,XMLForest(h_info.refdata_key1 "h_refdata_key1")
                    ,XMLForest(h_info.refdata_key2 "h_refdata_key2")
                    ,XMLForest(h_info.refdata_key3 "h_refdata_key3")
                    ,XMLForest(h_info.refdata_key4 "h_refdata_key4")
                    ,XMLForest(h_info.refdata_key5 "h_refdata_key5")
                    ,XMLForest(h_info.lineno "h_lineno")
                    ,XMLForest(h_info.info "h_info")
                    ,XMLForest(h_info.confirm_info "h_confirm_info")
                    ,XMLForest(h_info.allow_feedback "h_allow_feedback")
                    ,XMLForest(h_info.status "h_status")
                    ,XMLForest(h_info.timestamp "h_timestamp")
                    ,XMLForest(h_info.batchseq "h_batchseq")
                    ,XMLForest(h_info.errcode "h_errorcode")
                    ,XMLForest(h_info.errinfo "h_errinfo")
                    ) ORDER BY h_info.refdata_key5 desc
                  )
                ) ORDER BY r.bestellnr, r.klient
              ).GetClobVal() as XML_HEAD
from ss_kommbestr r
JOIN if_add_on_info h_info ON h_info.refdata_key1 = r.bestellnr
                            AND h_info.refdata_key2 = r.klient
                            AND h_info.refdata_type = 'GOORDER'
                            AND h_info.refdata_key4 is null
where r.bestellnr = V_BESTELLNR
  and r.klient = V_KLIENT
group by r.MANDANT
, r.BESTELLNR
, r.BESTELLTYP
, r.KUNDENNR
, r.KLIENT
, r.NAME
, r.KUERZEL
, r.PLZ
, r.ORT
, r.STRASSE
, r.LAND
, r.SPEDITEUR
, r.SPEDITEUR2
, to_char(r.VERLADEDATUM, 'YYYYMMDDHH24MISS')
, r.TOURNR
, r.AUSLIEFERER
, r.POSANZ
, r.INFO
, r.STATUS
, r.FEHLERCODE
, r.WATOR
, to_char(r.BEREITZEIT, 'YYYYMMDDHH24MISS')
, to_char(r.STARTZEIT, 'YYYYMMDDHH24MISS')
, to_char(r.ENDEZEIT, 'YYYYMMDDHH24MISS')
, r.PAUSE
, r.LAGERKOSTEN
, r.LIEFERSEQ
, r.LIEFERSEQSTATUS
, r.AUFTRAGGEBER
, r.PLANZEIT
, r.TIMESTAMP
, r.BATCHSEQ
, r.ERRCODE
, r.ERRINFO
, r.NAME2
, r.NAME3
, r.NAMEZUSATZ
, r.STRASSE2
, r.TRANSPORTID) KOPF,
(select MAX(pr.bestellnr) as bestellnr
        , MAX(pr.partnerklient) as partnerklient
        , XMLAgg(
            XMLElement(
                  "SS_KOMMBESTPR"
                  ,XMLForest(pr.mandant "i_mandant")
                  ,XMLForest(pr.bestellnr "i_bestellnr")
                  ,XMLForest(pr.posnr "i_posnr")
                  ,XMLForest(pr.bestelltyp "i_bestelltyp")
                  ,XMLForest(pr.referenznr "i_referenznr")
                  ,XMLForest(pr.lfdnr "i_lfdne")
                  ,XMLForest(pr.kundennr "i_kundennr")
                  ,XMLForest(pr.partnerklient "i_partnerklient")
                  ,XMLForest(pr.persnr "i_persnr")
                  ,XMLForest(pr.ersatzartikel "i_ersatzartikel")
                  ,XMLForest(pr.urposnr "i_urposnr")
                  ,XMLForest(pr.lonr "i_lonr")
                  ,XMLForest(pr.sammellonr "i_sammellonr")
                  ,XMLForest(pr.liefernr "i_liefernr")
                  ,XMLForest(pr.lsposnr "i_lsposnr")
                  ,XMLForest(pr.verladenr "i_verladenr")
                  ,XMLForest(pr.lieferseq "i_lieferseq")
                  ,XMLForest(pr.artnr "i_artnr")
                  ,XMLForest(pr.artklient "i_artklient")
                  ,XMLForest(pr.var "i_var")
                  ,XMLForest(pr.bestart "i_bestart")
                  ,XMLForest(pr.artikeltyp "i_artikeltyp")
                  ,XMLForest(pr.be1he "i_be1he")
                  ,XMLForest(pr.hebez "i_hebez")
                  ,XMLForest(pr.barcode "i_barcode")
                  ,XMLForest(pr.be1bez "i_be1bez")
                  ,XMLForest(pr.bestellmenge1 "i_bestellmenge1")
                  ,XMLForest(pr.istmenge1 "i_istmenge")
                  ,XMLForest(pr.storno1 "i_storno1")
                  ,XMLForest(pr.be2he "i_be2he")
                  ,XMLForest(pr.istmenge2 "i_istmenge2")
                  ,XMLForest(pr.storno2 "i_storno2")
                  ,XMLForest(pr.bruttogew "i_bruttogew")
                  ,XMLForest(pr.fehltyp "i_fehltyp")
                  ,XMLForest(pr.istmhd "i_istmhd")
                  ,XMLForest(pr.serialno "i_seriennr")
                  ,XMLForest(pr.charge "i_charge")
                  ,XMLForest(pr.info "i_info")
                  ,XMLForest(pr.wator "i_wator")
                  ,XMLForest(pr.buchtyp "i_buchtyp")
                  ,XMLForest(pr.rueckmeldtyp "i_rueckmeldtyp")
                  ,XMLForest(pr.lagerkosten "i_lagerkosten")
                  ,XMLForest(pr.leergut "i_leergut")
                  ,XMLForest(pr.erfasspersnr "i_erfasspersnr")
                  ,XMLForest(pr.status "i_status")
                  ,XMLForest(pr.timestamp "i_timestamp")
                  ,XMLForest(pr.batchseq "i_batchseq")
                  ,XMLForest(pr.errcode "i_errcode")
                  ,XMLForest(pr.errinfo "i_errinfo")
                  ,XMLForest(pr.preis1 "i_preis1")
                  ,XMLForest(pr.preis2 "i_preis2")
                  ,XMLForest(pr.barcode1 "i_barcode1")
                  ,XMLForest(pr.barcode2 "i_barcode2")
                  ,XMLForest(pr.barcode3 "i_barcode3")
                  ,XMLForest(pr.barcode4 "i_barcode4")
                  ,XMLForest(pr.barcode5 "i_barcode5")
                  ,XMLForest(pr.barcode6 "i_barcode6")
                  ,XMLForest(pr.barcode7 "i_barcode7")
                  ,XMLForest(pr.barcode8 "i_barcode8")
                  ,XMLForest(pr.barcode9 "i_barcode9")
                  ,XMLForest(pr.barcode10 "i_barcode10")
                  ,XMLAGG(
                    XMLElement(
                      "SS_IFADDON"
                      ,XMLForest('POSTXT' "i_aaa")
                      ,XMLForest(p_info.mandator "i_mandant")
                      ,XMLForest(p_info.refdata_type "i_refdata_type")
                      ,XMLForest(p_info.refdata_key1 "i_refdata_key1")
                      ,XMLForest(p_info.refdata_key2 "i_refdata_key2")
                      ,XMLForest(p_info.refdata_key3 "i_refdata_key3")
                      ,XMLForest(p_info.refdata_key4 "i_refdata_key4")
                      ,XMLForest(p_info.refdata_key5 "i_refdata_key5")
                      ,XMLForest(p_info.lineno "i_lineno")
                      ,XMLForest(p_info.info "i_info")
                      ,XMLForest(p_info.confirm_info "i_confirm_info")
                      ,XMLForest(p_info.allow_feedback "i_allow_feedback")
                      ,XMLForest(p_info.status "i_status")
                      ,XMLForest(p_info.timestamp "i_timestamp")
                      ,XMLForest(p_info.batchseq "i_batchseq")
                      ,XMLForest(p_info.errcode "i_errorcode")
                      ,XMLForest(p_info.errinfo "i_errinfo")
                    ) ORDER BY p_info.refdata_key5 desc
                  )
                ) ORDER BY r.bestellnr, pr.posnr
              ).GetClobVal() as XML_POS
from ss_kommbestr r
JOIN ss_kommbestpr pr ON pr.bestellnr = r.bestellnr
                    AND pr.partnerklient = r.klient
JOIN if_add_on_info p_info ON p_info.refdata_key1 = r.bestellnr
                            AND p_info.refdata_key2 = r.klient
                            AND p_info.refdata_type = 'GOORDER'
                            AND p_info.refdata_key4 = pr.posnr
where r.bestellnr = V_BESTELLNR
  and r.klient = V_KLIENT
group by pr.MANDANT
, r.bestellnr
, pr.BESTELLNR
, pr.POSNR
, pr.BESTELLTYP
, pr.REFERENZNR
, pr.LFDNR
, pr.KUNDENNR
, pr.PARTNERKLIENT
, pr.PERSNR
, pr.ERSATZARTIKEL
, pr.URPOSNR
, pr.LONR
, pr.SAMMELLONR
, pr.LIEFERNR
, pr.LSPOSNR
, pr.VERLADENR
, pr.LIEFERSEQ
, pr.ARTNR
, pr.ARTKLIENT
, pr.VAR
, pr.BESTART
, pr.ARTIKELTYP
, pr.BE1HE
, pr.HEBEZ
, pr.BARCODE
, pr.BE1BEZ
, pr.BESTELLMENGE1
, pr.ISTMENGE1
, pr.STORNO1
, pr.BE2HE
, pr.ISTMENGE2
, pr.STORNO2
, pr.BRUTTOGEW
, pr.FEHLTYP
, pr.ISTMHD
, pr.SERIALNO
, pr.CHARGE
, pr.INFO
, pr.WATOR
, pr.BUCHTYP
, pr.RUECKMELDTYP
, pr.LAGERKOSTEN
, pr.LEERGUT
, pr.ERFASSPERSNR
, pr.STATUS
, pr.TIMESTAMP
, pr.BATCHSEQ
, pr.ERRCODE
, pr.ERRINFO
, pr.PREIS1
, pr.PREIS2
, pr.BARCODE1
, pr.BARCODE2
, pr.BARCODE3
, pr.BARCODE4
, pr.BARCODE5
, pr.BARCODE6
, pr.BARCODE7
, pr.BARCODE8
, pr.BARCODE9
, pr.BARCODE10
, pr.TRACKINGID) POS
WHERE KOPF.bestellnr = POS.bestellnr
  AND KOPF.klient = POS.partnerklient;
  
return V_XMLOUTPUT;

EXCEPTION
  WHEN OTHERS THEN
    log_in_db.logerror('ERR',SQLERRM, 'F_SCH_SWORD_XML_KOMMBESTR', 'Erstellung XML: '||v_Logging);
    NULL;
    
END;